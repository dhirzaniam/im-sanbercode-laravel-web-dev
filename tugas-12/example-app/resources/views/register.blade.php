<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welc" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="nama"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lnma"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" id="Male" name="gender">
        <label for="male">Male</label><br>
        <input type="radio" id="Female" name="gender">
        <label>Female</label><br>
        <input type="radio" id="Other" name="gender">
        <label>other</label><br><br>
        <label>Nationaly:</label><br><br>
        <select name="Nationaly">
            <option value="1">Indonesia</option>
            <option value="2">Jepang</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bhs">Bahasa Indonesia<br>
        <input type="checkbox" name="bhs">English<br>
        <input type="checkbox" name="bhs">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up"></input>
    </form>
</body>
</html>