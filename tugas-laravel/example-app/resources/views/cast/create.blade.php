@extends('layout.master')
@section('judul')
    Tambah Cast
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-floating mb-3">
            <label for="inputname">Nama</label>
            <input type="text" class="form-control" id="inputname" placeholder="Nama" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-floating mb-3">
            <label for="inputemail">Umur</label>
            <input type="text" class="form-control" id="inputemail" placeholder="Umur" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-floating mb-3">
            <label form="floatingTextarea2">bio</label>
            <textarea class="form-control"id="floatingTextarea2" style="height: 100px" name="bio"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
