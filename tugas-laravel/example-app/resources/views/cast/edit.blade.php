@extends('layout.master')
@section('judul')
    Edit Cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-floating mb-3">
            <label for="inputname">Nama</label>
            <input type="text" class="form-control" value="{{$cast->nama}}" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-floating mb-3">
            <label for="inputemail">Umur</label>
            <input type="text" class="form-control" value="{{$cast->umur}}" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-floating mb-3">
            <label form="floatingTextarea2">bio</label>
            <textarea class="form-control"id="floatingTextarea2" style="height: 100px" name="bio">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/cast" class="btn btn-secondary">Kembali</a>
    </form>
@endsection
