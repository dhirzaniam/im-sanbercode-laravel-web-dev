@extends('layout.master')
@section('judul')
    Detail Data
@endsection

@section('content')
    <div class="card-body">
        <h3 class="mt-3">Nama : {{ $cast->nama }}</h3>
        <hr>
        <p class="mt-3">Umur : {{ $cast->umur }}</p>
        <hr>
        <p class="mt-3">bio : {{ $cast->bio }}</p>
        <hr>
        </br>
        <a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
    </div>
@endsection
