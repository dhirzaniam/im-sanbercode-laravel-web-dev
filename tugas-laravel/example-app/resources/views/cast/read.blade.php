@extends('layout.master')
@section('judul')
    Halaman Data
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $value->nama }}</td>
                    <td>
                        <form action="/cast/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/cast/{{ $value->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{ $value->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <p>Tidak ada data</p>
            @endforelse
        </tbody>
    </table>
@endsection
